---
layout: category
title: "Ideation area"
image: "pexels-pixabay-355948.jpg"
description: "Here we brainstorm and design, and together we'll Reimagine Social Media."
category:
  name: ideate
  tags: [vision, design, pattern, brainstorm]
  image: "brainstorming.svg"
related:
  - title: "Fediverse Futures on Lemmy"
    url: "https://lemmy.ml/c/fediversefutures"
  - title: "Fediverse Futures on SocialHub"
    url: "https://socialhub.activitypub.rocks/c/fediversity/fediverse-futures/58"
permalink: "/ideate.html"
---
Fediverse has so much potential, and most of it remains still untapped. We want to realize the vision of creating **"Social Networking Reimagined"** and here we'll explore new and exciting ideas. Together we'll go from brainstorming towards actual realization. From dreams to materialisation and concrete application designs. Here we imagine fediverse futures.

<!-- Photo by Pixabay from Pexels
     https://www.pexels.com/photo/clear-light-bulb-355948/
-->