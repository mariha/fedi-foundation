---
layout: category
category:
  name: learn
  tags: [tutorials, reference, guide, best-practice]
  image: "learn.svg"
related:
  - title: "Guide for New ActivityPub Implementers"
    url: "https://socialhub.activitypub.rocks/pub/guide-for-new-activitypub-implementers"
  - title: "ActivityPub W3C Recommendation"
    url: "https://www.w3.org/TR/activitypub/"
  - title: "ActivityStreams W3C Recommendation"
    url: "https://www.w3.org/TR/activitystreams-core/"
permalink: "/learn.html"
---
The Fediverse technology landscape is broad. Many standards and specifications define how applications should interoperate together. A multitude of technologies and programming languages are used. Ever more complexity is added. We strive to make it as easy as possible for anyone, regardless of skills, to discover this promising social space.