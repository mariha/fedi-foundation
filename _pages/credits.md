---
title: "Credits"
layout: page-sidebar
permalink: "/credits.html"
---

This website was designed and launched by [Arnold Schrijver](/people/aschrijver). Care has been taken to remove all trackers, javascript and external web requests. We will continue to work on site accessibility and design, and encourage anyone to report improvements to our [issue tracker](https://codeberg.org/fediverse/fedi-foundation/issues).

We owe much gratitude and like to thank all the other people that have helped make this site rock. They can be found in [CONTRIBUTORS](https://codeberg.org/fediverse/fedi-foundation/src/branch/main/CONTRIBUTORS.md) and/or in the site's [commit history](https://codeberg.org/fediverse/fedi-foundation/commits/branch/main).

Our [sourcecode](https://codeberg.org/fediverse/fedi-foundation) is hosted on [Codeberg](https://join.codeberg.org/), the wonderful organisation that actively supports Free and Open Source Software Development. Static output is temporarily hosted on [Github Pages](https://pages.github.com/) until the new [Codeberg Pages](https://codeberg.org/momar/codeberg-pages) with custom domain support is operational.

It would be lovely and wonderful if you want to contribute yourself! Read here:

- [How to contribute to the site's source code](https://codeberg.org/fediverse/fedi-foundation/src/branch/main/CONTRIBUTING.md)
- [How to publish pages to the Fedi Foundation](/publish.html)

Parts of the site design were inspired by these fine themes:

- [Mundana Jekyll Theme](https://github.com/wowthemesnet/mundana-theme-jekyll) is what this site is adapted from.
- [Minimal Mistakes Jekyll Theme](https://github.com/mmistakes/minimal-mistakes) inspired the people directory.
- [Just The Docs Jekyll Theme](https://github.com/pmarsceill/just-the-docs) inspired the `docs` layout page design.

We like to give due credit to the following projects and tools:

- [Jekyll](https://jekyllrb.com) static site generator.
- [ForkAwesome](https://github.com/ForkAwesome/Fork-Awesome) for providing a great icon collection.
- [Coolors](https://coolors.co/f5007e-222222-fdbe49-00f53d-2fd7da) for generating our palette.
- [Gradient Magic](https://www.gradientmagic.com/) for the CSS pattern in the page hero.
- [Manypixels](https://www.manypixels.co/gallery) for some decorative free SVG illustrations.
- [Pexels](https://www.pexels.com/) for some great free stock photos used in our posts.