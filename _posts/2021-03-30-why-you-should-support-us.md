---
title: "Why you should support us: Humane technology, Fediverse and the Future"
description: "We need your support for the healthy evolution of the Fediverse, a playground of humane technologists."
author: aschrijver
categories: community
tags: [support, vision]
status: featured
image: pexels-mododeolhar-5795688.jpg
---

The Fediverse has an enormous potential, but it is still weak and your help is needed. Created in true grassroots fashion, by the people and for the people, this online social fabric has risen to greate heights. But to reach the next stages in the Fediverse's evolution and concerted effort is needed. More cooperation and collaboration is vital.

<!-- Photo by mododeolhar from Pexels
     https://www.pexels.com/photo/people-reaching-hands-to-each-other-5795688/
 -->