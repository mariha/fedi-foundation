---
title: "Exploring the &laquo;Community Has No Boundary&raquo; paradigm"
description: "Communities are everywhere in daily life, intricately connected. So why are they so fragmented and isolated online?"
author: aschrijver
categories: ideate
tags: [design, patterns, brainstorm]
status: featured
image: pexels-pixabay-207896.jpg
---

#### Contents

- [Introduction](#introduction)
- [Problem](#problem)
- [Context](#context)
- [Discussion](#discussion)
  - [Community defined](#community-defined)
  - [What ActivityPub offers](#what-activitypub-offers)
- [Solution](#solution)
  - [Domain model](#domain-model)
  - [Vocabulary extension](#vocabulary-extension)

---

This Pattern document explores Community concepts for federated apps that more closely represent how people interact together in real life.

## Introduction

Communities are all around us. We participate in them all of the time. Our circle of friends, our family, school, work, sports clubs, the town where we live. All communities. And they are intricately interwoven and overlapping, forming a complex social fabric. Online we have communities too. Social media, forums, mailing lists, game worlds. But they aren't as intricately connected. Community concepts are usually bound by the application or server that hosts them.

## Problem

Defining a universal, interoperable Community model for the Fediverse is a challenge. There are:

- Numerous application-specific ways to express what constitutes a community.
- Numerous ways to define membership and inter-community relationships.
- Restrictions to a community model that result from current specification design.

## Context

The **"Community has no Boundary"** paradigm entails standardizing an ActivityPub vocabulary extension and federated message exchange patterns, that:

- Supports defining of generic community, member and relationship abstractions.
- Are meaningful and usable across many application and business domains.
- Allow arbitrary further extensions and specialization for these domains.
- Facilitates easy alignment to definition used by other technology initiatives.

## Discussion

#### Community defined

This dictionary definition comes closest to our needs with regards to defining Community:

{:start="3"}
3. [Community](https://www.thefreedictionary.com/community) _(The Free Dictionary)_
    - Similarity or identity: a community of interests.
    - Sharing, participation, and fellowship: a sense of community.

Note that _"Sense of community"_ is very important in Fediverse culture. Wikipedia uses the following definition of [**"Community"**](https://en.wikipedia.org/wiki/Community):

> A community is a social unit (a group of living things) with commonality such as norms, religion, values, customs, or identity.

At an implementation level we discern communities as [**"Social groups"**](https://en.wikipedia.org/wiki/Social_group): **_"Two or more people who interact with one another, share similar characteristics, and collectively have a sense of unity."_**

The interaction between people follows a [**"Social organization"**](https://en.wikipedia.org/wiki/Social_organization): **_"A pattern of relationships between and among individuals and social groups."_**

The relationships can be of [many different types](https://en.wikipedia.org/wiki/Outline_of_relationships), all forms of [**"Interpersonal relationships"**](https://en.wikipedia.org/wiki/Interpersonal_relationship): **_"Social associations, connections, or affiliations between two or more people."_**

#### What ActivityPub offers

The [W3C ActivityStreams 2.0](https://www.w3.org/TR/activitystreams-core) Recommendation provides us with:

- The abstract definition of [**Actors**](https://www.w3.org/TR/activitystreams-core/#actors) that can interact in a social network.
- [**Group**](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-group), [**Person**](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-person) and [**Organization**](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-organization) as meaningful actors in a Community.
- [**Application**](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-application) and [**Service**](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-service) actors _may_ be members too (e.g. a chatbot).
- [**Collection**](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-collection) and [**OrderedCollection**](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-orderedcollection) to define lists of items.
- [**Relationship**](https://www.w3.org/TR/activitystreams-vocabulary/#dfn-relationship) object to describe a relationship between two actors.
- [**VCard**](https://www.w3.org/TR/vcard-rdf/) SHOULD be used for additional Group, Person and Organization metadata.

`TODO`

## Solution

#### Domain model

`TODO`

#### Vocabulary extension

`TODO`

<!-- Photo by Pixabay from Pexels
     https://www.pexels.com/photo/adventure-backlit-dawn-dusk-207896/
 -->