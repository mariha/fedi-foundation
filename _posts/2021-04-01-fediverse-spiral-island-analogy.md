---
title: "Fediverse.. The Spiral Island Analogy"
description: "The Fediverse is like a hand-made island. A place of hope floating in rough seas of inhumane technology. Here, building from trash we create beauty. An archipelago can arise upon which humanity stands."
author: aschrijver
categories: community
tags: [vision]
status: featured
image: richart-sowa-spiral-island.jpeg
---

[Spiral Island](https://en.wikipedia.org/wiki/Spiral_Island) was created by Richart Sowa from stuff other people considered trash. By collecting and reusing discarded plastic bottles, Richart ever expanded its base. His vision was for there to be similar islands, all across the globe. An inspiring symbol of sustainability and hope, his dilligent work was nonetheless destroyed by a hurricane. Richart’s dedication in a way mirrors how we build our beloved Fediverse… but we must prepare so it'll be hurricane-proof.

---

The people that are passionately advocating and building the Fediverse…

They are a friendly bunch from all around the world having very diverse backgrounds and leading interesting, colourful lives. Online they are together as one. United in Diversity, these fedizens. The Fediverse they build are like small and fragile islands anchored precariously in the waves of society. Flourishing island communities, together they form an archipelago, where a unique culture thrives. It is worth protecting. It is unique. It is promising too, and deserves to grow.

Exposed to rough seas of inhumane technology and raging tides of Big Tech, the fedizens take great care to maintain and expand. Building things the way they like it best. There is freedom. A speck of beauty among dark waters and threatening clouds. There’s an economy on the islands. People exchanging thoughts and ideas, they inspire and encourage each other to make things better. Ever improving.

Like Spiral Island everything rests upon its floating foundation, the Fediverse technology base. It is mashed together by bits and pieces of knowledge re-applied - discarded bottles, that others made to trash, reused here… revived as Humane Technology. The island fedizens, techies and many, many others, work tirelessly to make things more buoyant and expand the living space. One such group of builders, of friends, they call themselves the [SocialHub](https://socialhub.activitypub.rocks). A great team that everyone can join to help and build and build.

Foundations are important, they are vital. They are the bedrock upon which we stand. Not all bottles in the island base stick well together all so easily. They need to be bound and tied together. If done well, then marvelous structures - our federated apps - can stand upon them and to never fall apart. There’ll be bridges between them, walkways and hanging gardens. Places of wonder to behold.

Builders and maintainers are important too, and those that support them in whatever ways. They allow the Fediverse archipelago to evolve ever further, and keep intact what already floats. Collaboration is key, and - while freedom reigns - some coordination is required here. So that balance is attained.

It is easy to find supporters, because the story of these great small islands is so inspiring. Turning trash into beauty. Do things the way they are meant to be done. Humanely, passionately and with vibrant life. A compelling story, this **Tale of the Fediverse**. It will naturally spread.

Just being on these islands, to participate and walk its winding paths, means one educates oneself on how things ought to be. No need to preach, or even to convince and force ideas on others.

Just follow the signs, hear our rallying cry. They read clearly and loud: Come join us in our quest..
Let’s imagine things together.

We offer [**“Social Networking Reimagined”**](/2021/04/social-networking-reimagined/) !

<!-- Image source: http://chicliving.com.mx/spiral-island-en-la-riviera-maya/ -->