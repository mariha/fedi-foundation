---
title: "Social Networking Reimagined.. What does that mean?"
description: "SocialHub and the Fedi Foundation will take a close look at how we can design social networks differently. Humane and for the People."
author: aschrijver
categories: community
tags: [vision, research]
status: featured
image: pexels-roman-odintsov-4925878.jpg
---

Social networks allow us to interact with others online. But _traditional_ social media platforms with their money-making incentives and dominant position, want to keep us there at all cost. The harms they cause to society cannot be overstated. We must find a different path! We need to reimagine social networking.

`TODO`

<!-- Photo by ROMAN ODINTSOV from Pexels
     https://www.pexels.com/photo/woman-in-black-crew-neck-t-shirt-and-brown-pants-sitting-on-gray-concrete-stairs-4925878/
 -->