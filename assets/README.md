# Style guide

## Colors

Colors of ActivityPub logo:

- purple: #f1007e
- grey: #6d6d6f

Colors of SocialHub dark theme:

- header: #111111
- background: #222222

Palette for FEDI logo:

- https://coolors.co/f1007e-222222-fdbe49-00f53d-2fd7da